# This script will take scrape a website and convert the text to speech

import requests
from bs4 import BeautifulSoup
from gtts import gTTS
import pygame

# Define URL
website_url = 'https://www.pointsincase.com/lists/reasons-why-this-page-was-left-intentionally-blank'

# HTTP get request
webpage_response = requests.get(website_url)

# Convert webpage_response to BeautifulSoup data frame
website_soup = BeautifulSoup(webpage_response.content, "html.parser")

# Global webpage_list
webpage_list = []
clean_webpage_list = []

# Grab the jokes about blank pages
for text in website_soup.find_all(attrs={'class': 'main'}):
    webpage_list.append(text.get_text())

# Clean up the list data
# Split on '\n'
webpage_list = webpage_list[0].split('\n')

# Remove references to '\n' at each index
for text in webpage_list:
    if text.replace('\n', '', 100) == '':
        webpage_list.pop(webpage_list.index(text))

# Store relevant data in clean_webpage_list
for text in webpage_list:
    if text == 'Related':
        break
    else:
        clean_webpage_list.append(text)
# clean_webpage_list.insert(0, '  ')

# Place contents of clean webpage_list into mp3
# For whatever reason, the first word is ignored
tts_text = 'l  ' + ' '.join(clean_webpage_list)

# Create TTS mp3 file
language = 'en'
tts_object = gTTS(text=tts_text, lang=language, slow=False)
tts_object.save('joke.mp3')
play_file = 'joke.mp3'

# Output audio from mp3 within Python
pygame.mixer.init()
pygame.mixer.music.load(play_file)
pygame.mixer.music.play()
while pygame.mixer.music.get_busy():
    pygame.time.Clock().tick()
